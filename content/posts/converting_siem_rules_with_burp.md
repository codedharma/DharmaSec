---
title: "Quickly Converting Suppression to Filtering Rules Using Burp in USM Anywhere"
date: 2018-04-27T15:50:39-05:00
draft: false
---

The suggested method for creating filtering rules in USM Anywhere is to start by creating suppression rules. Events that get suppressed by any given rule are still stored in USM Anywhere but get hidden from event views. The advantage with suppression rules is that you can first make sure that your rules work as expected before creating filtering rules, as filtered events do not get stored or processed. 

If you want to see if your suppression rules are working as expected, you would simply go to an events view and add a new filter called "Suppress Rule Name":

![image](/pictures/av_suppressrulename.png)

You would then click on "show suppressed", at which point you would want to verify that your rule is working as expected.

![image](/pictures/av_showsuppressed.png)

If your rule works as expected, you may want to create a filtering rule so that the same events do not get logged or processed. In order to do this you would have to create a new Filtering rule. You can of course create the rule manually, making sure that you replicate your suppression rule fields and values one by one. If that process seems a bit tedious  (specially for time consuming tuning efforts) you can also use Burp Suite to quickly convert your existing suppression rules into filtering rules. 

### Modifying Rules with Burp Suite

_Requirements_

1. Make sure to [configuring your browser](https://support.portswigger.net/customer/portal/articles/1783066-configuring-firefox-to-work-with-burp) to use Burp as a proxy.

2. You will need to [install Burp's CA certificate in your browser](https://support.portswigger.net/customer/portal/articles/1783075-Installing_Installing%20CA%20Certificate.html).

If you have completed the the above you can just proceed to the below steps.

1. Open the suppression rule. You can access your rules from `Settings`>`Rules`.

2. Make any changes you'd like to make. I usually change the rule name to indicate that it is a filtering rule. 

3. Make sure Burp is set to capture HTTP requests. In the proxy tab, make sure that `Intercept is On` is selected.

    ![Intercept is On](/pictures/burp_interceptison.png)

4. Save your rule. The request should be captured by Burp.

5. In in the request body, look for the `action` key and change the value from `suppression` to `drop`.

    ![Intercept is On](/pictures/converting_siem_rules_with_burp/av_burp_actionsuppression.png)

6. Click on `Intercept is On` to release the request.

Your rule should now be a filtering rule. If you want to convert existing filtering rules into suppression rules, just follow the same steps above but change `drop` to `suppression` instead in step 6.




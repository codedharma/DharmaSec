---
title: "A Simple InfoSec & AppSec Blog"
date: 2018-04-26T23:57:54-05:00
draft: false
---

This blog is meant to be a simple site that focuses on security and programming content. Rather than focusing on creating fancy sites I want to spend my time sharing information that I have found valuable. The main content that you will find is security focused (InfoSec & AppSec).

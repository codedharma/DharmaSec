#!/usr/bin/env bash

# exit on error
set -e

DIR=site
BUCKET=s3://codedharma.com
DIST_ID=$CODEDHARMA_CDN_DIST_ID
CACHE_ID=`date +%s`
FORMATTED_JSON='{"Paths": {"Quantity": 1,"Items": ["/*"]},"CallerReference":"'${CACHE_ID}'"}'

aws cloudfront create-invalidation --distribution-id $DIST_ID --invalidation-batch "$FORMATTED_JSON"